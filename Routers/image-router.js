let router = require("express").Router();
var multer = require("multer");
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./assets");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname); // modified here  or user file.mimetype
  }
});
var upload = multer({ storage: storage });

const imageController = require("../Controllers/ImageController");

router.get("/", imageController.getImage);
router.post("/", upload.single("avatar"), imageController.addImage);

module.exports = router;

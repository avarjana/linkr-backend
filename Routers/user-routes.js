let router = require('express').Router();

const userController = require('../Controllers/UserController')

//router.post('/', userController.addUser);
router.post('/', userController.getUserByEmail); //query -> email=
router.post('/update', userController.updateUser); //query -> id=
router.post('/verify', userController.verifyUser); //query -> id=
router.get('/getAll', userController.getAllUsers); 

module.exports = router;
let router = require("express").Router();
let multer = require("multer");

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ IMAGE ROUTES //\\//\\//\\-----------------------------------------------------------------

const imageController = require("../Controllers/ImageController");

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./assets");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname); // modified here  or user file.mimetype
  }
});
let upload = multer({ storage: storage });

router.get("/image", imageController.getImage);
router.post("/image", upload.single("file"), imageController.addImage);

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ POST ROUTES //\\//\\//\\------------------------------------------------------------------

const postController = require("../Controllers/PostController");

router.post("/post/add", postController.add);
router.post("/post/update", postController.update);
router.get("/post/get/:id", postController.getById);
router.get("/post/delete/:id", postController.deleteById);

router.get("/post/user/:uid", postController.getByUserId); // page, limit
router.get("/post/project/:pid", postController.getByProjectId); // page, limit
router.get("/post/all", postController.getPosts); // page, limit

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ PROJECT ROUTES //\\//\\//\\

const projectController = require("../Controllers/ProjectController");

router.post("/project/add", projectController.add);
router.post("/project/update", projectController.update);
router.get("/project/get/:id", projectController.getById);
router.get("/project/delete/:id", projectController.deleteById);

router.get("/project/all", projectController.getProjects); // page, limit
router.get("/project/:avenue", projectController.getByAvenue); // page, limit
router.get("/project/:pid/chairs", projectController.getChairs);
router.post("/project/search", projectController.getSearchProjects); // page, limit

router.get("/project/subscribe/:pid/:uid", projectController.subscribe);
router.get("/project/unsubscribe/:pid/:uid", projectController.unsubscribe);
router.get("/project/subscribed/:pid/:uid", projectController.subscribed);
router.get(
  "/project/subscribedProjects/:uid",
  projectController.userSubscribedProjects
);

router.get("/project/:pid/subscribers", projectController.getSubscribers);
router.get("/project/:pid/attendance", projectController.getAttendance);
router.post("/project/markAttendance", projectController.markAttendance);

router.get("/project/user/:uid", projectController.userOwnedProjects); // project ownerships to add posts

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ USER ROUTES //\\//\\//\\

const userController = require("../Controllers/UserController");

router.post("/user/add", userController.add);
router.post("/user/update", userController.update);
router.get("/user/get/:id", userController.getById);
router.get("/user/delete/:id", userController.deleteById);

router.post("/user/login", userController.login);
router.get("/user/get/email/:email", userController.getByEmail);
router.get("/user/verify/:uid", userController.verify);
router.get("/user/requests", userController.getRequests); // page, limit

router.get("/user/getStarted/:email", userController.getStarted);
router.post("/user/search", userController.search);
router.post("/user/password", userController.updatePassword);
router.get("/user/securityQuestion/:email", userController.getSecurityQuestion);

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ NOTIFICATION ROUTES //\\//\\//\\

const notificationController = require("../Controllers/NotificationController");

router.post("/notification/add", notificationController.add);
router.post("/notification/update", notificationController.update);
router.get("/notification/get/:id", notificationController.getById);
router.get("/notification/delete/:id", notificationController.deleteById);

router.get("/notification/user/:uid", notificationController.getByUserId); // page, limit

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ CONTRIBUTION ROUTES //\\//\\//\\

const contributionController = require("../Controllers/ContributionController");

router.post("/contribution/add", contributionController.add);
router.post("/contribution/update", contributionController.update);
router.get("/contribution/get/:id", contributionController.getById);
router.get("/contribution/delete/:id", contributionController.deleteById);

router.get("/contribution/:cid/heart/:add", contributionController.heart);
router.get("/contribution/project/:pid", contributionController.getByProject); // page, limit

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ ENDORSEMENT ROUTES //\\//\\//\\

const endorsementController = require("../Controllers/EndorsementController");

router.post("/endorsement/:cid/add", endorsementController.add);
router.post("/endorsement/update", endorsementController.update);
router.get("/endorsement/get/:id", endorsementController.getById);
router.get("/endorsement/delete/:id", endorsementController.deleteById);

router.get(
  "/endorsement/contribution/:cid",
  endorsementController.getContributionEndorsements
); // page, limit

//-----------------------------------------------------------------------------------------------------

//\\//\\//\\ TASK ROUTES //\\//\\//\\

const taskController = require("../Controllers/TaskController");

router.post("/task/add", taskController.add);
router.post("/task/update", taskController.update);
router.get("/task/get/:id", taskController.getById);
router.get("/task/delete/:id", taskController.deleteById);

router.get("/task/user/added/:uid", taskController.getByAddedUserId); // page, limit
router.get("/task/user/assigned/:uid", taskController.getByAssignedUserId); // page, limit
router.get(
  "/task/user/assigned/requests/:uid",
  taskController.getRequestsByAssignedUserId
); // page, limit
router.get(
  "/task/user/assigned/pending/:uid",
  taskController.getPendingByAssignedUserId
); // page, limit
router.get(
  "/task/user/assigned/completed/:uid",
  taskController.getCompletedByAssignedUserId
);

router.get("/task/accept/:tid/:uid", taskController.acceptTask);
router.get("/task/completed/:tid", taskController.completed);

// page, limit
router.get("/task/project/pending/:pid", taskController.getPendingByProject); // page, limit
router.get("/task/project/taken/:pid", taskController.getTakenByProject); // page, limit

router.get("/task/project/:pid", taskController.getByProjectId); // page, limit

//-----------------------------------------------------------------------------------------------------

module.exports = router;

let router = require('express').Router();

const projectController = require('../Controllers/ProjectController')

//router.post('/', userController.addUser);
router.post('/', projectController.addProject);
router.get('/', projectController.getAllProjects); //req query -> page
router.post('/update',projectController.updateProject);

module.exports = router;
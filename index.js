let express = require("express");
let bodyParser = require("body-parser");
let mongoose = require("mongoose");

let app = express();

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());
// mongoose.connect("mongodb://localhost/resthub", { useNewUrlParser: true });
mongoose.connect(
  "mongodb://avarjana:rotaractmora19@ds255857.mlab.com:55857/heroku_mgzp6tdq",
  { useNewUrlParser: true }
);

let db = mongoose.connection;
let port = process.env.PORT || 8080;

let routes = require("./Routers/router");

app.use("/api", routes);

app.use("/assets", express.static(__dirname + "/assets"));

app.listen(port, function () {
  console.log("Running RestHub on port " + port);
});

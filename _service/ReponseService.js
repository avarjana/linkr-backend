exports.sendError = (res, msg, err) => {
    res
        .status(500)
        .json({
            status: "Failed",
            message: msg,
            payload: err
        });
};

exports.sendSuccess = (res, msg, data) => {
    res
        .status(200)
        .json({
            status: "Success",
            message: msg,
            payload: data
        });
};

exports.handleResponse = (res, err, payload) => {
    if (err) {
        this.sendError(res, 'Error occurred', err)
    } else if (payload == null) {
        this.sendError(res, 'Not found', payload)
    } else {
        this.sendSuccess(res, 'Request success', payload)
    }
};

const responseService = require("../_service/ReponseService");
const Notification = require("../Models/notification");
var ObjectId = require("mongoose").Types.ObjectId;

exports.add = (req, res) => {
  let post = new Notification(req.body);

  post.save((err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.update = (req, res) => {
  let post = new Notification(req.body);

  Notification.findByIdAndUpdate(
    req.body._id,
    post,
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.getById = (req, res) => {
  Notification.findById(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.deleteById = (req, res) => {
  Notification.findByIdAndDelete(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.getByUserId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  let notifications = [];

  Notification.find(
    {
      $or: [
        {
          $and: [
            { user: new ObjectId(req.params.uid) },
            { type: { $ne: "Project" } }
          ]
        },
        {
          type: "Project"
        }
      ]
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 })
    .populate("project")
    .populate("task")
    .populate("endorsement")
    .populate("post");
};

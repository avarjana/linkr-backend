const responseService = require("../_service/ReponseService");
const Task = require("../Models/task");

var ObjectId = require("mongoose").Types.ObjectId;
const ProjectSubscriber = require("../Models/project_subscribers");

exports.add = (req, res) => {
  let post = new Task(req.body);

  post.save((err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.update = (req, res) => {
  let post = new Task(req.body);

  Task.findByIdAndUpdate(
    req.body._id,
    post,
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.getById = (req, res) => {
  Task.findById(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.deleteById = (req, res) => {
  Task.findByIdAndDelete(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.acceptTask = (req, res) => {
  Task.findById(req.params.tid, (err, payload) => {
    if (payload.assigned_user == null) {
      payload.assigned_user = new ObjectId(req.params.uid);
      payload.isAccepted = true;
      payload.save((err, p) => responseService.handleResponse(res, err, p));
    } else {
      responseService.handleResponse(res, err, "taken");
    }
  });
};

exports.completed = (req, res) => {
  Task.findByIdAndUpdate(
    req.params.tid,
    {
      isCompleted: true
    },
    (err, p) => responseService.handleResponse(res, err, p)
  );
};

exports.getByAddedUserId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Task.find(
    {
      added_user: req.params.uid
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getByAssignedUserId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);
  //test
  Task.find(
    {
      assigned_user: req.params.uid
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getRequestsByAssignedUserId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  ProjectSubscriber.find(
    {
      user: req.params.uid
    },
    (err, val) => {
      let projs = [];
      val.forEach(v => {
        projs.push(v.project);
      });
      Task.find(
        {
          isAccepted: false,
          project: { $in: projs }
        },
        (err, payload) => responseService.handleResponse(res, err, payload)
      )
        .skip((page - 1) * limit)
        .limit(limit)
        .sort({ created_at: -1 });
    }
  );

  //test
};

exports.getPendingByProject = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  //test
  Task.find(
    {
      project: req.params.pid,
      isAccepted: false
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getTakenByProject = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  //test
  Task.find(
    {
      project: req.params.pid,
      isAccepted: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getPendingByAssignedUserId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  //test
  Task.find(
    {
      assigned_user: req.params.uid,
      isAccepted: true,
      isCompleted: false
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getCompletedByAssignedUserId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  //test
  Task.find(
    {
      assigned_user: req.params.uid,
      isCompleted: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getByProjectId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Task.find(
    {
      project: req.params.pid
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

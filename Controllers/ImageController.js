fs = require("fs");
var path = require("path");

exports.addImage = function(req, res, next) {
  // req.file is the `avatar` file
  console.log(req.file);
  if (req.file) {
    res.status(200).json({
      status: "Success",
      message: "Image added",
      image:
        "https://link-r-mobile.herokuapp.com/assets/" + req.file.originalname
    });
  } else {
    res.status(500).json({
      status: "Failed",
      message: "Failed adding image"
    });
  }
  // req.body will hold the text fields, if there were any
};

exports.getImage = function(req, res) {
  var file = path.join("../image/acc30b3a8599310b5fa391f3b1bf7eae");
  var s = fs.createReadStream(file);
  s.on("open", function() {
    res.set("Content-Type", type);
    s.pipe(res);
  });
};

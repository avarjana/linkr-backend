const responseService = require("../_service/ReponseService");
const User = require("../Models/user");
const bcrypt = require("bcrypt");

const saltRounds = 10;

exports.add = (req, res) => {
  let user = new User(req.body);
  user.save((err, payload) => {
    if (err) {
      responseService.handleResponse(res, err, null);
    } else {
      payload.pass = null;
      payload.securityQuestion = null;
      payload.answer = null;
      responseService.handleResponse(res, err, payload);
    }
  });
  /* let p = "";
   bcrypt.hash(user.pass, saltRounds, function(err, hash) {
    console.log(hash);
    user.pass = hash;
    user.save((err, payload) => {
      if (err) {
        responseService.handleResponse(res, err, null);
      } else {
        payload.pass = null;
        payload.securityQuestion = null;
        payload.answer = null;
        responseService.handleResponse(res, err, payload);
      }
    });
  }); */
};

exports.login = (req, res) => {
  User.findOne({ email: req.body.email }, (err, payload) => {
    if (payload == null) {
      this.add(req, res);
    } else {
      responseService.handleResponse(res, err, payload);
    }
    /* if (err || payload == null) {
      res.status(200).json({
        status: "Failed",
        message: "Invalid credentials",
        payload: null
      });
    } else {
      console.log(payload.pass);
      bcrypt.hash(req.body.pass, saltRounds, function(err, hash) {
        console.log(hash);
        bcrypt.compare(req.body.pass, payload.pass, function(err, r) {
          if (r) {
            payload.pass = null;
            payload.securityQuestion = null;
            payload.answer = null;
            responseService.handleResponse(res, err, payload);
          } else
            res.status(200).json({
              status: "Failed",
              message: "Invalid credentials",
              payload: null
            });
        });
      });
    } */
  });
};

exports.update = (req, res) => {
  let post = new User(req.body);

  User.findByIdAndUpdate(
    req.body._id,
    post,
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.search = (req, res) => {
  User.find(

    { name: { $regex: ".*" + req.body.term + ".*", $options: "-i" } },

    /* {
      $or: [
        { fname: { $regex: "/^" + req.body.term + "/i" } },
        { lname: { $regex: "/^" + req.body.term + "/i" } }
      ]
    }, */
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .sort({ fname: 1 })
    .select("name image_url")
    .limit(5);
};

exports.getById = (req, res) => {
  User.findById(req.params.id, (err, payload) => {
    payload.pass = null;
    payload.securityQuestion = null;
    payload.answer = null;
    responseService.handleResponse(res, err, payload);
  });
};

exports.deleteById = (req, res) => {
  User.findByIdAndDelete(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.getByEmail = (req, res) => {
  User.find({ email: req.params.email }, (err, payload) => {
    payload.pass = null;
    payload.securityQuestion = null;
    payload.answer = null;
    responseService.handleResponse(res, err, payload);
  });
};

exports.updatePassword = (req, res) => {
  bcrypt.hash(req.body.pass, saltRounds, function (err, hash) {
    User.findOneAndUpdate(
      req.body.email,
      {
        pass: hash
      },
      {
        new: true
      },
      (err, payload) => responseService.handleResponse(res, err, payload)
    );
  });
};

exports.getSecurityQuestion = (req, res) => {
  console.log(req.params.email);
  User.find({ email: req.query.email }, (err, payload) => {
    responseService.handleResponse(res, err, {
      securityQuestion: payload.securityQuestion,
      answer: payload.answer
    });
  });
};

const userController = require("../Controllers/UserController");

exports.getStarted = (req, res) => {
  //console.log(req);
  User.find({ email: req.params.email }, function (err, payload) {
    if (!payload.length) {
      req.body.email = req.params.email;
      userController.add(req, res);
    } else {
      responseService.handleResponse(res, err, payload);
    }
  });
};

exports.verify = (req, res) => {
  User.findByIdAndUpdate(
    req.params.uid,
    {
      verified: true
    },
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.getRequests = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  User.find(
    {
      verified: false
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: 1 });
};

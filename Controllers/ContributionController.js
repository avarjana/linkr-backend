const responseService = require("../_service/ReponseService");
const Contribution = require("../Models/contribution");

exports.add = (req, res) => {
  let post = new Contribution(req.body);

  post.save((err, payload) =>
    responseService.handleResponse(res, err, payload.populate('user'))
  );
};

exports.update = (req, res) => {
  let post = new Contribution(req.body);

  Contribution.findByIdAndUpdate(
    req.body._id,
    post,
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.getById = (req, res) => {
  Contribution.findById(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.deleteById = (req, res) => {
  Contribution.findByIdAndDelete(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.heart = (req, res) => {
  Contribution.findByIdAndUpdate(
    req.params.cid,
    req.params.add === "add"
      ? {
        $inc: { hearts: 1 }
      }
      : {
        $inc: { hearts: -1 }
      },
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.getByProject = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Contribution.find({ project: req.params.pid }, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 })
    .populate('user');
};

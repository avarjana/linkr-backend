const responseService = require("../_service/ReponseService");
const Project = require("../Models/project");
const ProjectSubscriber = require("../Models/project_subscribers");
var ObjectId = require("mongoose").Types.ObjectId;

const Task = require("../Models/task");
const Noti = require("../Models/notification");

exports.add = (req, res) => {
  console.log(req.body);
  let project = new Project(req.body);

  project.save(function(err, payload) {
    if (err) {
      console.log(err);
      responseService.handleResponse(res, err, payload);
    }
    console.log(payload);
    let projectSubscriber = new ProjectSubscriber({
      user: payload.user,
      project: payload._id,
      role: "Owner"
    });
    projectSubscriber.save((err, payload) => {
      if (err) {
        console.log(err);
        return;
      }
    });
    let noti = new Noti({
      type: "Project",
      project: payload._id,
      description:
        "A new project has been added to " +
        payload.avenue +
        " avenue, named " +
        payload.name
    });
    noti.save((err, qs) => {
      responseService.handleResponse(res, err, payload);
    });
  });

  project.chairs.forEach(function(chair, index) {
    let projectSubscriber = new ProjectSubscriber({
      user: chair,
      project: project.id,
      role: "Chair"
    });
    projectSubscriber.save((err, ps) => {});
  });
};

exports.update = (req, res) => {
  let project = new Project(req.body);

  console.log(req.body);
  Project.findByIdAndUpdate(
    req.body._id,
    project,
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.getById = (req, res) => {
  Project.findById(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  ).populate("chairs", "fname lname");
};

exports.deleteById = (req, res) => {
  let p;
  Project.findByIdAndDelete(req.params.id, function(err, payload) {
    responseService.handleResponse(res, err, payload);
    p = payload;
  });

  //post
};

exports.getProjects = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Project.find((err, payload) =>
    responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .populate("chairs", "fname lname")
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getByAvenue = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Project.find(
    {
      avenue: req.param.avenue
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .populate("chairs", "fname lname")
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getSearchProjects = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Project.find(
    {
      name: new RegExp(req.body.search, "i")
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .populate("chairs", "fname lname")
    .sort({ created_at: -1 });
};

exports.userOwnedProjects = (req, res) => {
  ProjectSubscriber.find(
    {
      user: new ObjectId(req.params.uid),
      $or: [{ role: "Owner" }, { role: "Chair" }]
    },
    "project -_id",
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .populate("project")
    .populate("chairs", "fname lname");
};

//\\----------------------SUBSCRIBE------------------------//\\

exports.getChairs = (req, res) => {
  ProjectSubscriber.find(
    {
      project: req.params.pid,
      role: "Chair"
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  ).populate("user");
};

exports.subscribe = (req, res) => {
  let subscribe = new ProjectSubscriber({
    project: new ObjectId(req.params.pid),
    user: new ObjectId(req.params.uid),
    role: "Follower"
  });

  subscribe.save((err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.unsubscribe = (req, res) => {
  ProjectSubscriber.findOneAndDelete(
    {
      project: new ObjectId(req.params.pid),
      user: new ObjectId(req.params.uid)
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.subscribed = (req, res) => {
  ProjectSubscriber.findOne(
    {
      project: new ObjectId(req.params.pid),
      user: new ObjectId(req.params.uid)
    },
    (err, payload) => {
      if (payload) responseService.handleResponse(res, err, true);
      else responseService.handleResponse(res, err, false);
    }
  );
};
exports.userSubscribedProjects = (req, res) => {
  ProjectSubscriber.find(
    {
      user: new ObjectId(req.params.uid)
    },
    "project -_id",
    (err, payload) => {
      let fin = [];
      payload.forEach(p => {
        fin.push(p.project);
      });
      responseService.handleResponse(res, err, fin);
    }
  ).populate("project");
};

exports.getSubscribers = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  ProjectSubscriber.find(
    {
      project: req.params.pid
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getAttendance = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  ProjectSubscriber.find(
    {
      project: req.params.pid,
      attended: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.markAttendance = (req, res) => {
  ProjectSubscriber.findOneAndUpdate(
    {
      user: req.body.user,
      project: req.body.project
    },
    {
      attended: true
    },
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

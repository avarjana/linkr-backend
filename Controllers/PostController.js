const responseService = require("../_service/ReponseService");
const Post = require("../Models/post");

exports.add = (req, res) => {
  let post = new Post(req.body);

  post.save((err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.update = (req, res) => {
  let post = new Post(req.body);

  Post.findByIdAndUpdate(
    req.body._id,
    post,
    {
      new: true
    },
    (err, payload) => responseService.handleResponse(res, err, payload)
  );
};

exports.getById = (req, res) => {
  Post.findById(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  ).populate("user");
};

exports.deleteById = (req, res) => {
  Post.findByIdAndDelete(req.params.id, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  );
};

exports.getByUserId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Post.find({ user: req.params.uid }, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getByProjectId = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Post.find({ project: req.params.pid }, (err, payload) =>
    responseService.handleResponse(res, err, payload)
  )
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

exports.getPosts = (req, res) => {
  let page = parseInt(req.query.page);
  let limit = parseInt(req.query.limit);

  Post.find((err, payload) => responseService.handleResponse(res, err, payload))
    .skip((page - 1) * limit)
    .limit(limit)
    .sort({ created_at: -1 });
};

const responseService = require("../_service/ReponseService");
const Endorsement = require('../Models/endorsement');

exports.add = (req, res) => {

    let post = new Endorsement(req.body);

    post.save(
        (err, payload) => responseService.handleResponse(res,err,payload)
    );
};

exports.update = (req, res) => {

    let post = new Endorsement(req.body);

    Endorsement.findByIdAndUpdate(
        req.body._id,
        post,
        {
            new: true
        },
        (err, payload) => responseService.handleResponse(res,err,payload)
    );
};

exports.getById = (req, res) => {
    Endorsement.findById(
        req.params.id,
        (err, payload) => responseService.handleResponse(res,err,payload)
    )
};

exports.deleteById = (req, res) => {
    Endorsement.findByIdAndDelete(
        req.params.id,
        (err, payload) => responseService.handleResponse(res,err,payload)
    )
};

exports.getContributionEndorsements = (req, res) => {
    let page = parseInt(req.query.page);
    let limit = parseInt(req.query.limit);

    Endorsement.find(
        {
          contribution : req.params.cid
        },
        (err, payload) => responseService.handleResponse(res, err, payload)
    )
        .skip((page - 1) * limit)
        .limit(limit)
        .sort({created_at: -1})
};
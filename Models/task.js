var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");

var schema = mongoose.Schema(
  {
    added_user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user"
    },
    assigned_user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user"
    },
    added_user_name: String,
    added_user_role: String,
    projectName: String,
    deadline: Date,
    isAccepted: {
      type: Boolean,
      default: false
    },
    type: {
      // Project | General
      type: String
    },
    project: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "project"
    },
    description: {
      type: String
    },
    isCompleted: {
      type: Boolean,
      default: false
    },
    confirmCompleted: {
      type: Boolean,
      default: false
    }
  },
  {
    timestamps: {
      createdAt: "created_at"
    }
  }
);

module.exports = mongoose.model("task", schema);
schema.plugin(uniqueValidator);

var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var schema = mongoose.Schema(
    {
        type: {
            type: String // post | project | endorse | task
        },
        project: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "project"
        },
        post: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "post"
        },
        task: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "task"
        },
        endorsement: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "endorsement"
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "user"
        },
        description: {
            type: String
        },
    },
    {
        timestamps: {
            createdAt: 'created_at'
        }
    }
);

module.exports = mongoose.model('notification', schema);
schema.plugin(uniqueValidator);


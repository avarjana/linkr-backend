var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");

var schema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user"
    },
    name: {
      type: String,
      unique: true
    },
    year: {
      type: Number
    },
    phase: {
      type: String
    },
    description: {
      type: String
    },
    image_url: {
      type: String
    },
    avenue: {
      type: String
    },
    is_active: {
      type: Boolean,
      default: true
    },
    chairs: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
      }
    ]
  },
  {
    timestamps: {
      createdAt: "created_at"
    }
  }
);

module.exports = mongoose.model("project", schema);
schema.plugin(uniqueValidator);

/*
name: {
    type: String,
    required: true,
    unique: true
},
project: { type: mongoose.Schema.Types.ObjectId, ref: "project" },
chairs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }]
*/

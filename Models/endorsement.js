var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var schema = mongoose.Schema(
    {
        user: { 
            type: mongoose.Schema.Types.ObjectId, 
            ref: "user" 
        },
        contribution : {
            type: mongoose.Schema.Types.ObjectId,
            ref: "contribution"
        },
        description: {
            type: String
        }
    },
    {
        timestamps: {
            createdAt: 'created_at'
        }
    }
);

module.exports = mongoose.model('endorsement', schema);
schema.plugin(uniqueValidator);



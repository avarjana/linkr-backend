var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");

var schema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user"
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "project"
  },
  role: {
    type: String // Follower, Chair, Owner
  },
  attended: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model("project_subscribe", schema);
schema.plugin(uniqueValidator);

var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");

var schema = mongoose.Schema(
  {
    name: {
      type: String
    },
    image_url: {
      type: String
    },
    email: {
      type: String,
      unique: true
    },
    mobile: {
      type: String
    },
    whatsapp: {
      type: String
    },
    verified: {
      type: Boolean,
      default: false
    },
    role: {
      //member | dir | president (etc)
      type: String,
      default: "Member"
    },
    userType: {
      // board | member | admin
      type: String,
      default: "Member"
    },
    gender: {
      type: String
    },
    birthday: {
      type: Date
    },
    bio: {
      type: String
    },
    faculty: {
      type: String
    },
    department: {
      type: String
    }
  },
  {
    timestamps: {
      createdAt: "created_at"
    }
  }
);

module.exports = mongoose.model("user", schema);
schema.plugin(uniqueValidator);

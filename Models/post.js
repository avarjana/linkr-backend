const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

let schema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user"
    },
    project: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "project"
    },
    added_by: {
      type: String
    },
    project_name: {
      type: String
    },
    image_url: {
      type: String
    },
    description: {
      type: String
    },
    title: {
      type: String
    }
  },
  {
    timestamps: {
      createdAt: "created_at"
    }
  }
);

module.exports = mongoose.model("post", schema);
schema.plugin(uniqueValidator);

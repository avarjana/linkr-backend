var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var schema = mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "user"
        },
        project: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "project"
        },
        description: {
            type: String
        },
        hearts: {
            type: Number,
            default: 0
        },
        accepted: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: {
            createdAt: 'created_at'
        }
    }
);

module.exports = mongoose.model('contribution', schema);
schema.plugin(uniqueValidator);


/*
name: {
    type: String,
    required: true,
    unique: true
},
project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "project"
},
chairs: [
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }
]
*/